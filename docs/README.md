# AI-Int-poc

This demo will try to demonstrate the end to end developer experience which will be committed to 90 days plan of DigitalX.

## Project Flow
This Proof-of-concept initiates the Enngineers journey from Backstage to spin up a Open Shift Devspace in Sandpbox environment. The Journey has covers following
-  The Devspace environment has got CodeGpt enabled to aid devleopers in AI assited coding.
-  The IDE will be integrated with the Developers Git repo in Gitlab, so any code changes can be committed directly into the repote repo.
-  There will be a runner spinned at the time of Devspace creation, which will be reghistered to the Devlopers Project. So any changes committed will trigger a CI pipeline.
-  The CI pipeline includes build, test, secret scanning, SAST scanning, code quality scans.
-  The Code will publish a wiki page with the details of Project's health which should help in determining whether the project is eligible for straight through processing.

## Backstage Deployment locally

Kindly follow below steps to deploy **Backstage** on localhost/laptop:
* Make sure the local system has all the pre-requisite fulfilled as mentioned in https://backstage.io/docs/getting-started/#prerequisites
* Run the command `npx @backstage/create-app@latest` will install Backstage. The wizard will create a subdirectory inside your current working directory.
* When the installation is complete you can go to the application directory and start the app: <br>
    `cd <backstage application directory>` <br>
    `yarn dev`
* It will open a webpage on your default browser at `http://localhost:3000`
* To setup authentication of the local deployment with GitLab, Open *app-config.yaml*, and add your *clientId* and *clientSecret* to this file like below:
```
auth:
  # see https://backstage.io/docs/auth/ to learn about auth providers
  environment: development
  providers:
    gitlab:
      development:
        clientId: YOUR CLIENT ID
        clientSecret: YOUR CLIENT SECRET
```
* Setting a GitLab Integration by providing a *personal token* created on GitLab.
```integrations:
  gitlab:
    - host: gitlab.com
      token: glpat-xxxxxxxxxxxx # this should be the token from GitLab
```
* All done, now you can **Login to Backstage**.

## Opening a Containerized IDE with AI Assissted Code Suggestion Enablement
**Option-1:** 
Whenever a catalog created, on the summary page, you will find a hyperlinked URL for IDE which will create and open a Containerized IDE on https://devspaces.apps.sandbox-m3.1530.p1.openshiftapps.com.


**Option-2:** 
* Under the `Catalog`, search and open a newly created catalog on backstage.
* Click on `View Source` which will open a Repository on GitLab.
* On the README.md page, you will find a magiclink to create and open a containerized IDE with sample structure of a repo.

On the created IDE, you will find various files, mainly devfile.yaml, .gitlab-ci.yaml, extensions.json and settings.json under .vscode directory.
Below are the brief description of each file. 

For the demo, we are managing all the code at this GitLab Repo: https://gitlab.com/backdevgit/aiint-poc/tree/main

### devfile.yaml
This file is being used to do the below:
* Take in the repository hosting your application source code.
* Build your code.
* Run your application on your local container.
* Deploy your application to cloud-native containers.

For further details, kindly refer the resources available at https://devfile.io/docs/2.2.0/what-is-a-devfile


### .vscode/extensions.json
This file is used when you want to install some recommended extensions on the IDE. 
For this demo, we are installing Gitlab workflow, Python and CodeGPT extensions.
```
 {
    "recommendations": [
      "ms-python.python",
      "DanielSanMedium.dscodegpt",
      "GitLab.gitlab-workflow"
    ]
  }
```

### .vscode/settings.json
It is a file that contains the settings for Visual Studio Code. It can be used to configure the user settings or the workspace settings for different projects.
For this demo, we are setting the value of the AI Assisted Code Suggestion option to `true`.
```
{
    "gitlab.aiAssistedCodeSuggestions.enabled": true
}
```
### .gitlab-ci.yaml
In this file, we can define the scripts, configuration files, templates, dependencies and caches.
For our demo, we will register a gitlab runner as part of devfile task and it will trigger the pipeline which will eventually publish a dashboard on GitLab Wiki.
```
# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
#default:
#  tags:
#    - demowiki
image: curlimages/curl:latest
stages:
- test
- wiki

sast:
  stage: test
include:
- template: Security/SAST.gitlab-ci.yml
- template: Security/Secret-Detection.gitlab-ci.yml
- template: Code-Quality.gitlab-ci.yml

wiki:
  stage: wiki
  script:
    - echo "Creating Wiki Page for Demo"
    - "curl -X POST --fail -F token=glptt-1afcfddabcc9995c23717ee612972db68c9ad7be -F ref=main -F variables[PROJECT_ID]=$CI_PROJECT_ID -F variables[PROJECT_NAME]=$CI_PROJECT_NAME https://gitlab.com/api/v4/projects/46952197/trigger/pipeline"
```