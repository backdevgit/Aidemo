import requests
import datetime
import sys

# GitLab API endpoint and personal access token
GITLAB_API_URL = 'https://gitlab.com/api/v4/'


# ID of the project
PROJECT_ID = sys.argv[1]
print(PROJECT_ID)
PROJECT_NAME = sys.argv[2]
print(PROJECT_NAME)
PERSONAL_ACCESS_TOKEN = sys.argv[3]
PIPELINE_COUNT = 4
WIKI_PROJECT_ID = sys.argv[4]
lines_of_code = 0

# Get lines of code

from code_stats import count_lines_of_code

def count_lines(directory):
    total_lines = 0
    
    for root, dirs, files in os.walk(directory):
        for file in files:
            file_path = os.path.join(root, file)
            if file_path.endswith('.py'): # Change the extension as per your requirement
                lines = count_lines_of_code(file_path)
                total_lines += lines
                
    return total_lines

# Provide the path to the directory you want to count lines of code
directory_path = '/builds/backdevgit/AIIntdemo'
lines_of_code = count_lines(directory_path)
print(f'Total lines of code: {lines_of_code}')
# Get project details
headers = {'Private-Token': PERSONAL_ACCESS_TOKEN}
project_url = f'{GITLAB_API_URL}projects/{PROJECT_ID}'
wiki_project_url = f'{GITLAB_API_URL}projects/{WIKI_PROJECT_ID}'
project_data = requests.get(project_url, headers=headers).json()

# Get number of commits
commits_url = f'{project_url}/repository/commits?pagination=keyset&per_page=100&order_by=id&sort=asc'
commits_data = requests.get(commits_url, headers=headers).json()
commit_count = len(commits_data)

# Get pipeline count
pipelines_url = f'{project_url}/pipelines?pagination=keyset&per_page=100&order_by=id&sort=desc'
pipelines_data = requests.get(pipelines_url, headers=headers).json()
pipeline_count = len(pipelines_data)

# Get latest pipeline details
latest_pipeline = pipelines_data[0]
last_pipeline_status = latest_pipeline['status']
pipeline_start_time = latest_pipeline['created_at']
pipeline_end_time = latest_pipeline['updated_at']
pipeline_execution_time = datetime.datetime.strptime(pipeline_end_time, '%Y-%m-%dT%H:%M:%S.%fz') - datetime.datetime.strptime(pipeline_start_time, '%Y-%m-%dT%H:%M:%S.%fz')
pipeline_execution_days = pipeline_execution_time.days
pipeline_execution_hours, remainder = divmod(pipeline_execution_time.seconds, 3600)
pipeline_execution_minutes, pipeline_execution_seconds = divmod(remainder, 60)

# Get latest pipeline details
latest_pipelines = pipelines_data[:PIPELINE_COUNT]
pipeline_statuses = [pipeline['status'] for pipeline in latest_pipelines]

# Check if Code Quality scan got completed successfully
last_pipeline_jobs_url = f'{project_url}/pipelines/{latest_pipeline["id"]}/jobs'
last_pipeline_jobs_data = requests.get(last_pipeline_jobs_url, headers=headers).json()
specific_job = next((job for job in last_pipeline_jobs_data if job['name'] == 'code_quality'), None)
code_quality_status = specific_job['status'] if specific_job else 'Not Found'

# Check if SAST scan got completed successfully
specific_job = next((job for job in last_pipeline_jobs_data if job['name'] == 'semgrep-sast'), None)
sast_status = specific_job['status'] if specific_job else 'Not Found'

# Check if Secret detection scan got completed successfully
specific_job = next((job for job in last_pipeline_jobs_data if job['name'] == 'secret_detection'), None)
secret_detection_status = specific_job['status'] if specific_job else 'Not Found'

# Get security scanning details
security_scanning_url = f'{project_url}/vulnerabilities'
security_scanning_data = requests.get(security_scanning_url, headers=headers).json()

if isinstance(security_scanning_data, list):
    secret_detection_count = len(security_scanning_data)
    severities = [vulnerability.get('severity') for vulnerability in security_scanning_data]
    has_severity = any(severities)
else:
    secret_detection_count = 1 if security_scanning_data else 0
    has_severity = security_scanning_data.get('severity')

secret_detection_status = 'Failed' if has_severity else 'Success'

# Create wiki page content
wiki_content = f'''
# {PROJECT_NAME} Details

| Metric                      | Value         |
|-----------------------------|---------------|
| Number of Commits           | {commit_count}     |
| Pipeline Count              | {pipeline_count}     |
| Total lines of code.        | {lines_of_code}     |
| Last Pipeline Status        | {last_pipeline_status}     |
| Last 4 Pipeline Status      | {' '.join(pipeline_statuses)}     |
| Code Quality Scan           | {code_quality_status}     |
| SAST Scan                   | {sast_status}     |
| Secret Detection Count      | {secret_detection_count}     |
| Secret Detection Status     | {secret_detection_status}     |
| Pipeline Execution Time     | {pipeline_execution_days} days {pipeline_execution_hours} hours {pipeline_execution_minutes} minutes {pipeline_execution_seconds} seconds     |
'''
# Create or update the wiki page
wiki_create_url = f'{wiki_project_url}/wikis'
wiki_list = requests.get(wiki_create_url, headers=headers).json()
if wiki_list:
    wiki_page_url = f'{wiki_project_url}/wikis/{wiki_list[0]["slug"]}'
    wiki_update_payload = {
        'content': wiki_content
    }
    requests.put(wiki_page_url, headers=headers, json=wiki_update_payload)
    print(f'Wiki page updated: {wiki_page_url}')
else:
    wiki_page_url = f'{wiki_project_url}/wikis'
    wiki_create_payload = {
        'title': 'Metrics',
        'content': wiki_content
    }
    requests.post(wiki_page_url, headers=headers, json=wiki_create_payload)
    print(f'Wiki page created: {wiki_page_url}')
